# Comentarios e ideas

Tener una página inicio sencillita y bien pensada desee el punto de vista de marketing y poner el ampliar información para pasar a las subpaginas
Me tiene muy muy despistado lo del blog. No se ni como enfocarlo porque no tengo ejemplos de los layouts que pueden estar disponibles...a mi el típico de Blogspot o wordspace no me llama, pero es lo que hay
Y estaba pensando que en la página de presentación de equipo, con nosotros de cabecera, le gustaría hacer una pantalla partida con fotos y una breve descripción, con una barra en el medio que se desplazara derecha o izquierda y desplegara el texto más extenso de currículum de cada unoz tapando la foto del que no corresponda
Luego en segunda.linea haría marcos horizontales con colaboradores, en plan foto o imagen a un lado y una descripción al otro, con enlaces o info de contacto
El logo lo voy a rediseñar un nada... Igual quitarle un punto de grosor y de hacerle más estilizado el palo de la d
Y jugaré con la escala por si lo pongo más cuadradote de proporción... Más estrecho en general y los palos largos más altos
Aún tengo que verlo en pantalla y ver si me convence
Mi idea era tenerlo con transparencia para que saliera superpuesto a un carrusel de imágenes de presentación
También estaba pensando en usar letra century gothic
No sé si es web safe

Calibro, óptima o Cambria a priori

Paleta dsn
~~~~
#CACDCE
#3F9AB8
#207BA1
#8B8B8B
#2C5B72
#949494

#040404
#636567
#1F3C4E
#322B34
#062D39
#948C8C

CSS

/* CSS HEX */
--light-gray: #cacdceff;
--spanish-gray: #949494ff;
--dim-gray: #636567ff;
--raisin-black: #322b34ff;
--rich-black-fogra-39: #040404ff;
--blue-green: #3f9ab8ff;
--celadon-blue: #207ba1ff;
--charcoal: #1f3c4eff;
--silver-sand: #c4d4d5ff;
--middle-yellow: #ffe600ff;

/* CSS HSL */
--light-gray: hsla(195, 4%, 80%, 1);
--spanish-gray: hsla(0, 0%, 58%, 1);
--dim-gray: hsla(210, 2%, 40%, 1);
--raisin-black: hsla(287, 9%, 19%, 1);
--rich-black-fogra-39: hsla(0, 0%, 2%, 1);
--blue-green: hsla(195, 49%, 48%, 1);
--celadon-blue: hsla(198, 67%, 38%, 1);
--charcoal: hsla(203, 43%, 21%, 1);
--silver-sand: hsla(184, 17%, 80%, 1);
--middle-yellow: hsla(54, 100%, 50%, 1);

/* SCSS HEX */
$light-gray: #cacdceff;
$spanish-gray: #949494ff;
$dim-gray: #636567ff;
$raisin-black: #322b34ff;
$rich-black-fogra-39: #040404ff;
$blue-green: #3f9ab8ff;
$celadon-blue: #207ba1ff;
$charcoal: #1f3c4eff;
$silver-sand: #c4d4d5ff;
$middle-yellow: #ffe600ff;

/* SCSS HSL */
$light-gray: hsla(195, 4%, 80%, 1);
$spanish-gray: hsla(0, 0%, 58%, 1);
$dim-gray: hsla(210, 2%, 40%, 1);
$raisin-black: hsla(287, 9%, 19%, 1);
$rich-black-fogra-39: hsla(0, 0%, 2%, 1);
$blue-green: hsla(195, 49%, 48%, 1);
$celadon-blue: hsla(198, 67%, 38%, 1);
$charcoal: hsla(203, 43%, 21%, 1);
$silver-sand: hsla(184, 17%, 80%, 1);
$middle-yellow: hsla(54, 100%, 50%, 1);

/* SCSS RGB */
$light-gray: rgba(202, 205, 206, 1);
$spanish-gray: rgba(148, 148, 148, 1);
$dim-gray: rgba(99, 101, 103, 1);
$raisin-black: rgba(50, 43, 52, 1);
$rich-black-fogra-39: rgba(4, 4, 4, 1);
$blue-green: rgba(63, 154, 184, 1);
$celadon-blue: rgba(32, 123, 161, 1);
$charcoal: rgba(31, 60, 78, 1);
$silver-sand: rgba(196, 212, 213, 1);
$middle-yellow: rgba(255, 230, 0, 1);

/* SCSS Gradient */
$gradient-top: linear-gradient(0deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-right: linear-gradient(90deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-bottom: linear-gradient(180deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-left: linear-gradient(270deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-top-right: linear-gradient(45deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-bottom-right: linear-gradient(135deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-top-left: linear-gradient(225deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-bottom-left: linear-gradient(315deg, #cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);
$gradient-radial: radial-gradient(#cacdceff, #949494ff, #636567ff, #322b34ff, #040404ff, #3f9ab8ff, #207ba1ff, #1f3c4eff, #c4d4d5ff, #ffe600ff);

CODE

/* CSV */
cacdce,949494,636567,322b34,040404,3f9ab8,207ba1,1f3c4e,c4d4d5,ffe600

/* Array */
["cacdce","949494","636567","322b34","040404","3f9ab8","207ba1","1f3c4e","c4d4d5","ffe600"]

/* Object */
{"Light Gray":"cacdce","Spanish Gray":"949494","Dim Gray":"636567","Raisin Black":"322b34","Rich Black FOGRA 39":"040404","Blue Green":"3f9ab8","Celadon Blue":"207ba1","Charcoal":"1f3c4e","Silver Sand":"c4d4d5","Middle Yellow":"ffe600"}

/* Extended Array */
[{"name":"Light Gray","hex":"cacdce","rgb":[202,205,206],"cmyk":[2,0,0,19],"hsb":[195,2,81],"hsl":[195,4,80],"lab":[82,-1,-1]},{"name":"Spanish Gray","hex":"949494","rgb":[148,148,148],"cmyk":[0,0,0,42],"hsb":[0,0,58],"hsl":[0,0,58],"lab":[61,0,0]},{"name":"Dim Gray","hex":"636567","rgb":[99,101,103],"cmyk":[4,2,0,60],"hsb":[210,4,40],"hsl":[210,2,40],"lab":[43,0,-1]},{"name":"Raisin Black","hex":"322b34","rgb":[50,43,52],"cmyk":[4,17,0,80],"hsb":[287,17,20],"hsl":[287,9,19],"lab":[19,5,-5]},{"name":"Rich Black FOGRA 39","hex":"040404","rgb":[4,4,4],"cmyk":[0,0,0,98],"hsb":[0,0,2],"hsl":[0,0,2],"lab":[1,0,0]},{"name":"Blue Green","hex":"3f9ab8","rgb":[63,154,184],"cmyk":[66,16,0,28],"hsb":[195,66,72],"hsl":[195,49,48],"lab":[60,-17,-24]},{"name":"Celadon Blue","hex":"207ba1","rgb":[32,123,161],"cmyk":[80,24,0,37],"hsb":[198,80,63],"hsl":[198,67,38],"lab":[48,-12,-28]},{"name":"Charcoal","hex":"1f3c4e","rgb":[31,60,78],"cmyk":[60,23,0,69],"hsb":[203,60,31],"hsl":[203,43,21],"lab":[24,-5,-14]},{"name":"Silver Sand","hex":"c4d4d5","rgb":[196,212,213],"cmyk":[8,0,0,16],"hsb":[184,8,84],"hsl":[184,17,80],"lab":[84,-5,-2]},{"name":"Middle Yellow","hex":"ffe600","rgb":[255,230,0],"cmyk":[0,10,100,0],"hsb":[54,100,100],"hsl":[54,100,50],"lab":[91,-9,90]}]

~~~~

