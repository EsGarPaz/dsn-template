// Set flag to include Preflight conditionally based on the build target.
const includePreflight = ( 'editor' === process.env._TW_TARGET ) ? false : true;

module.exports = {
	presets: [
		// Manage Tailwind Typography's configuration in a separate file.
		require( './tailwind-typography.config.js' ),
	],
	content: [
		// Ensure changes to PHP files and `theme.json` trigger a rebuild.
		'./theme/**/*.php',
		'./theme/theme.json',
	],
	safelist: [
		{
			pattern: /.*dgray.*/,
		},
		{
			pattern: /.*dblue.*/,
		},
		{
			pattern: /.*dyellow.*/,
		},
	],
	theme: {
		fontFamily: {
			sans: '"Century Gothic",Calibri,ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
			serif: 'ui-serif, Georgia, Cambria, "Times New Roman", Times, serif',
			mono: 'ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace',
		},
		extend: {
			colors: {
				dgray: {
					50: '#fdf8f6',
					100: '#f0f3f4',
					200: '#d7dadc',
					300: '#cacdce',
					400: '#949494',
					500: '#636567',
					600: '#4a484d',
					700: '#322b34',
					800: '#1b171c',
					900: '#040404',
				},
				dblue: {
					50: '#e1e9ea',
					100: '#c4d4d5',
					200: '#a2c5cd',
					300: '#81b7c6',
					400: '#60a8bf',
					500: '#3f9ab8',
					600: '#2f8aac',
					700: '#207ba1',
					800: '#1f5b77',
					900: '#1f3c4e',
				},
				dyellow: '#ffe600',
			},
		},
	},
	corePlugins: {
		// Disable Preflight base styles in CSS targeting the editor.
		preflight: includePreflight,
	},
	plugins: [
		// Add Tailwind Typography.
		require( '@tailwindcss/typography' ),

		// Extract colors and widths from `theme.json`.
		require( '@_tw/themejson' )( require( '../theme/theme.json' ) ),

		// Uncomment below to add additional first-party Tailwind plugins.
		// require( '@tailwindcss/aspect-ratio' ),
		// require( '@tailwindcss/forms' ),
		// require( '@tailwindcss/line-clamp' ),
	],
};
