<?php
/*
	Template Name: Home Page
*/

get_header(); ?>

	<?php get_template_part('template-parts/home/section','presentacion'); ?>

	<?php get_template_part('template-parts/home/section','servicios'); ?>

	<?php get_template_part('template-parts/home/section','proyectos'); ?>

	<?php get_template_part('template-parts/home/section','equipo'); ?>

<?php get_footer(); ?>
