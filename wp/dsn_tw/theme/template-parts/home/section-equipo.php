<?php
/*
	Template Name: Home Page proyectos list
*/?>

<section id="seccion-equipo" class="bloque-web my-3 bg-dgray-50">
    <h1>Conocenos</h1>

	<h2>El equipo</h2>
	<div id="ctEquipo" class="grid grid-cols-1 pr-3 lg:grid-cols-2 gap-6">
	  	<?php
		$argsOwns = array(
			'post_type' => 'equipo',
			'meta_key' => 'team_relation',
    		'meta_value'   => 'Miembro',
			'order' => 'ASC',
			'orderby' => 'menu_order'
		);
		$ownQuery = new WP_Query( $argsOwns );
		$odd = True;
		?>

			<?php while ( $ownQuery->have_posts() ) : $ownQuery->the_post(); ?>
			<div class="cardPersona bg-white md:bg-inherit overflow-hidden md:rounded-none rounded-lg w-full p-3 flex flex-col md:flex-row group">
				<?php if($odd): ?>
					<div class="w-1/2 mt-2 mx-auto overflow-hidden md:w-2/5 rounded-full border-dblue-600 border-2 md:rounded-lg shadow-lg md:order-1">
						<?php the_post_thumbnail('full', ['class' => 'object-cover w-full h-full']); ?>
					</div>
					<div class="w-full bg-white border-dblue-600 md:border-y-2 md:group-even:border-l-2 md:group-even:rounded-l-lg md:group-odd:border-r-2 md:group-odd:rounded-r-lg md:w-3/5 text-left p-6 md:my-6 space-y-2 md:order-2">
				<?php else: ?>
					<div class="w-1/2 mt-2 mx-auto overflow-hidden md:w-2/5 rounded-full border-dblue-600 border-2 md:rounded-lg shadow-lg md:order-2">
						<?php the_post_thumbnail('full', ['class' => 'object-cover w-full h-full']); ?>
					</div>
					<div class="w-full bg-white border-dblue-600 md:border-y-2 md:group-even:border-l-2 md:group-even:rounded-l-lg md:group-odd:border-r-2 md:group-odd:rounded-r-lg md:w-3/5 text-left p-6 md:my-6 space-y-2 md:order-1">
				<?php endif; $odd = !$odd;  ?>
						<p class="text-xl text-gray-700 font-bold"><?php the_title(); ?></p>
						<p class="text-base text-gray-400 italic"><?php the_field('team_jobs'); ?></p>
						<p class="text-sm text-gray-500"><?php has_excerpt() ? the_excerpt(): print ''; ?></p>
						<div class="flex justify-center space-x-2">
						<?php if ( the_field('team_facebook') ): ?>
							<a href="<?php the_field('team_facebook'); ?>" class="text-gray-500 hover:text-gray-600">
								<svg class="w-6 h-6" viewBox="0 0 32 32">
									<use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-facebook"></use>
								</svg>
							</a>
						<?php endif; ?>

						<?php if ( the_field('team_instagram') ): ?>
							<a href="<?php the_field('team_instagram'); ?>" class="text-gray-500 hover:text-gray-600">
								<svg class="w-6 h-6" viewBox="0 0 32 32">
									<use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-instagram"></use>
								</svg>
							</a>
						<?php endif; ?>

						<?php if ( the_field('team_twitter') ): ?>
							<a href="<?php the_field('team_twitter'); ?>" class="text-gray-500 hover:text-gray-600">
								<svg class="w-6 h-6" viewBox="0 0 32 32">
									<use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-twitter"></use>
								</svg>
							</a>
						<?php endif; ?>

						</div>
					</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>
	<div id="ctColaboradores">
		<h2>Colaboradores</h2>
        <p>Tambien colaboramos con profesionales excelentes que nos ayudan a llevar a buen termino los proyectos:</p>
		<?php
		$argsColab = array(
			'post_type' => 'equipo',
			'meta_key' => 'team_relation',
    		'meta_value'   => 'Colaborador',
			'order' => 'ASC',
			'orderby' => 'menu_order'
		);
		$ColabQuery = new WP_Query( $argsColab );
		?>

		<div class="flex flex-nowrap space-x-2 m-2 overflow-hidden">
			<?php while ( $ColabQuery->have_posts() ) : $ColabQuery->the_post(); ?>

			<div class="w-80 flex flex-col flex-none bg-white transition-colors duration-200 transform border cursor-pointer rounded-lg shadow-lg hover:border-transparent group hover:bg-dblue-100 dark:border-dgray-700 dark:hover:border-transparent">
            <div class="text-center p-2">
              <p class="text-base text-dgray-900 font-bold"><?php the_title(); ?></p>
              <p class="text-sm text-dgray-500 italic"><?php the_field('team_jobs'); ?></p>
            </div>
            <div class="w-40 self-center m-2">
			  <?php the_post_thumbnail('full', ['class' => 'object-center rounded-full object-cover ring-4 ring-dgray-400 group-hover:ring-dblue-400']); ?>
            </div>
            <div class="px-4">
              <p class="text-sm leading-relaxed text-dgray-500 font-normal">
			  <?php if ( has_excerpt() ) { the_excerpt(); } else { print ''; } ?>
              </p>
              <div class="flex justify-center space-x-2">
			  	<?php if ( the_field('team_facebook') ): ?>
					<a href="#<?php the_field('team_facebook'); ?>" class="text-gray-500 hover:text-gray-600">
						<svg class="w-6 h-6" viewBox="0 0 32 32">
							<use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-facebook"></use>
						</svg>
					</a>
				<?php endif; ?>

				<?php if ( the_field('team_instagram') ): ?>
					<a href="#<?php the_field('team_instagram'); ?>" class="text-gray-500 hover:text-gray-600">
						<svg class="w-6 h-6" viewBox="0 0 32 32">
							<use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-instagram"></use>
						</svg>
					</a>
				<?php endif; ?>

				<?php if ( the_field('team_twitter') ): ?>
					<a href="#<?php the_field('team_twitter'); ?>" class="text-gray-500 hover:text-gray-600">
						<svg class="w-6 h-6" viewBox="0 0 32 32">
							<use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-twitter"></use>
						</svg>
					</a>
				<?php endif; ?>

              </div>
            </div>


          </div>
			<?php endwhile; wp_reset_query(); ?>
		</div>
	</div>
	<div class="text-right p-5">
        <a class="btn-dblue-dark" href="./servicios/">Saber + </a>
      </div>
</section>
