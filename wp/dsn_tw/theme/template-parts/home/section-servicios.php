<?php
/*
	Template Name: Home Page servicios list
*/?>

<?php
	$args = array(
		'post_type' => 'servicios',
		'posts_per_page' => 6,
		'order' => 'DESC',
		'orderby' => 'menu_order'
	);
	$the_query = new WP_Query( $args );
?>


<section id="seccion-servicios" class="bloque-web my-3 bg-dgray-50 ">
      <h1 class="text-dblue-100 underline">Nuestros servicios</h1>
      <div class="flex flex-wrap" id="ctServicios">
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<div class="opacity-95 w-100 md:w-1/2 xl:w-1/3 flex mb-2 mt-2">
		<div class="max-w-sm px-8 py-4 mx-auto mt-16 rounded-lg shadow-lg shadow-black bg-white dark:bg-gray-800 border-2 border-dblue-500 dark:border-dblue-200">
		  <div class="flex justify-center -mt-20 md:justify-end ">
			<div class="w-32 h-32 flex justify-center bg-white dark:bg-gray-800 border-2 border-dblue-500 rounded-full dark:border-dblue-200">
				<svg class="<?php the_field('image_cssclass'); ?>">
					<use xlink:href="<?php get_template_directory_uri() ?>/assets/lib/icomoon/symbol-defs.svg#<?php the_field('image_name'); ?>"></use>
				</svg>
			</div>
		  </div>
		  <div class="md:-mt-10">
			<h2 class="mt-2 text-2xl font-semibold text-gray-800 dark:text-white md:mt-0 md:text-3xl"><?php the_title(); ?></h2>
			<p class="mt-2 text-gray-600 dark:text-gray-200"><?php if ( has_excerpt() ) { the_excerpt(); } else { the_content(); } ?></p>
		  </div>
		</div>
	  </div>
	<?php endwhile; wp_reset_query(); ?>
	</div>
	<div class="text-right p-5">
        <a class="btn-dblue-dark" href="./servicios/">Saber + </a>
      </div>
</section>
