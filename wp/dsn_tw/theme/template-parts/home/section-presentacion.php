<?php
$pres_titulo = get_field('pres_titulo');
$pres_cuerpo = get_field('pres_cuerpo');
?>
<section id="seccion-presentacion" class="bloque-web my-3 bg-white ">
	<?php if( !empty($pres_titulo) ) : ?>
		<h2><?php echo $pres_titulo; ?></h2>

		<?php endif; ?>
	<div id ="seccion_presentacion_cuerpo">
		<?php echo $pres_cuerpo; ?>
	</div>

</section>
