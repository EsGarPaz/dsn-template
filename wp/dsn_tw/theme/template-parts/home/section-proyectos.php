<?php
/*
	Template Name: Home Page proyectos list
*/?>

<?php
	$args = array(
		'post_type' => 'proyectos',
		'posts_per_page' => 6,
		'order' => 'ASC',
		'orderby' => 'menu_order'
	);
	$the_query = new WP_Query( $args );
?>

<section id="seccion-proyectos" class="bloque-web my-3 bg-dgray-50">
    <h1>Proyectos anteriores</h1>
    <div id="ctProyectos" class="p-6 grid grid-cols-3 gap-6">
	 <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <div class="cardProyecto flex flex-col relative overflow-hidden bg-white rounded-lg shadow-lg dark:bg-gray-800 group">
          <div class="relative w-full">
			  <?php the_post_thumbnail('full', ['class' => 'object-cover w-full h-full']); ?>
          </div>
          <div class="absolute px-2 py-2 bg-white -bottom-0 group-hover:bottom-0 w-full group-hover:h-full   opacity-90 border-4 text-center">
            <div class="tracking-widest block text-2xl font-bold text-gray-800 dark:text-white"><?php the_title(); ?></div>
            <div class="hidden pt-6 group-hover:block text-sm text-gray-700 dark:text-gray-200"><?php if ( has_excerpt() ) { the_excerpt(); } else { echo ''; } ?>
            </div>
          </div>
        </div>
		<?php endwhile; wp_reset_query(); ?>

	</div>
	<div class="text-right p-5">
       	<a class="btn-dblue-dark" href="./proyectos/">Saber + </a>
    </div>
</section>

