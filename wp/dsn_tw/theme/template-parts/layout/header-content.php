<?php
/**
 * Template part for displaying the header content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dsn
 */

?>
<?php
/*
<header id="masthead">
	<div>
		<?php
		the_custom_logo();
		if ( is_front_page() ) :
			?>
			<h1><?php bloginfo( 'name' ); ?></h1>
			<?php
		else :
			?>
			<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
		endif;
		$dsn_description = get_bloginfo( 'description', 'display' );
		if ( $dsn_description || is_customize_preview() ) :
			?>
			<p><?php echo $dsn_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
		<?php endif; ?>
	</div>

	<nav id="site-navigation">
		<button aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'dsn_tw' ); ?></button>
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			)
		);
		?>
	</nav><!-- #site-navigation -->
</header>#masthead
*/
?>

<header>
    <nav class="bg-gradient-to-b from-dblue-900 via-dblue-800 to-dblue-700 text-dgray-100">
      <div class="flex flex-wrap justify-between content-center">
        <div><a href="#" class="hover:text-white">
            <svg version="1.1" viewBox="0 0 14.037 9.0391" xmlns="http://www.w3.org/2000/svg"
              class="w-[90px] fill-white">
              <path
                d="m13.322 8.3197c-0.62019-0.66144-0.67942-1.1612-0.6997-1.9297l-0.01481-0.71472c0.01139-0.98161-0.98008-1.7382-1.7076-1.7449l-5.1674 3e-3c-0.071886 0-0.077131 0.14405 0.0025 0.14433h2.4382c0.40612-0.00903 0.84254 0.3038 0.83034 0.87865s-0.44426 0.80951-0.88257 0.83762h-7.5342c-0.0041114-1.7314 1.4968-2.5836 2.5253-2.6155 0.35322-0.01264 0.5951 0.00742 0.9516 0.13249v-2.8188l0.78934-5.5559e-4 -0.0025002 4.1856c-0.64161-0.59249-0.86796-0.74834-1.5373-0.75273-1.0723 0.01589-1.5233 0.64452-1.7516 1.0593l6.7125 0.011279c0.098383-0.0025 0.0996-0.13665 0.00531-0.13664h-2.5531c-0.3343-0.017363-0.71936-0.33636-0.72645-0.84218-0.0070839-0.50579 0.32914-0.87374 0.82968-0.87529l4.9979-0.00278c1.4029 0.02903 2.5919 1.0845 2.5926 2.7589l-0.0011 2.5077-0.09689-0.085312zm-4.1834-4.242h0.78356s0.00222 0.76512 0 1.0193c-0.00222 0.25421-0.29558 0.67439-0.78356 0.67811zm0.00658-1.0755c0.0072267-0.44192 0.29333-0.78073 0.84424-0.78073h2.6315c-0.0048 0.32508-0.25084 0.78309-0.75714 0.78073z" />
            </svg>
          </a>
        </div>
        <div class="md:hidden self-center">DSÑ Estudio</div>
        <button data-collapse-toggle="mobile-menu" type="button"
          class="inline-flex items-center p-2 ml-3 text-sm rounded-lg md:hidden hover:bg-dblue-100 focus:outline-none focus:ring-2 focus:ring-dblue-200 dark:text-dgray-400 dark:hover:bg-dgray-700 dark:focus:ring-dgray-600"
          aria-controls="mobile-menu-2" aria-expanded="false">
          <span class="sr-only">Abrir menú principal</span>
          <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
              clip-rule="evenodd"></path>
          </svg>
          <svg class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clip-rule="evenodd"></path>
          </svg>
        </button>
        <!-- <button data-collapse-toggle="mobile-menu" type="button" class="inline-flex items-center p-2 ml-3 text-sm text-dgray-200 rounded-lg md:hidden hover:bg-dgray-100 focus:outline-none focus:ring-2 focus:ring-dgray-200 dark:text-dgray-400 dark:hover:bg-dgray-700 dark:focus:ring-dgray-600" aria-controls="mobile-menu-2" aria-expanded="false">
           <span class="sr-only">Open main menu</span>
           <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
           <svg class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
         </button> -->
        <div class="hidden w-full md:block md:w-auto md:inline-flex items-center" id="mobile-menu">

          <div
            class="block py-2 pr-4 pl-3 border-b border-dblue-300 hover:bg-dblue-100 md:hover:bg-transparent md:border-0 md:hover:text-dblue-700">
            <a href="#" aria-current="page">Principal</a>
          </div>
          <div
            class="block py-2 pr-4 pl-3 border-b border-dblue-300 hover:bg-dblue-100 md:hover:bg-transparent md:border-0 md:hover:text-dblue-700">
            <a href="#">Quienes somos</a>
            <!-- <a href="#" class="block py-2 pr-4 pl-3 text-dgray-700 border-b border-dgray-100 hover:bg-dgray-50 md:hover:bg-transparent md:border-0 md:hover:text-dblue-700 md:p-0 dark:text-dgray-400 md:dark:hover:text-white dark:hover:bg-dgray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-dgray-700">Quienes somos</a> -->
          </div>
          <div
            class="block py-2 pr-4 pl-3 border-b border-dblue-300 hover:bg-dblue-100 md:hover:bg-transparent md:border-0 md:hover:text-dblue-700">
            <a href="#">Lo que hacemos</a>
          </div>
          <div
            class="block py-2 pr-4 pl-3 border-b border-dblue-300 hover:bg-dblue-100 md:hover:bg-transparent md:border-0 md:hover:text-dblue-700">
            <a href="#">Blog</a>
          </div>
          <div
            class="block py-2 pr-4 pl-3 border-b border-dblue-300 hover:bg-dblue-100 md:hover:bg-transparent md:border-0 md:hover:text-dblue-700">
            <a href="#">Redes Sociales</a>
          </div>
        </div>
      </div>
    </nav>
    <div class="w-full flex justify-center bg-white">
      <img class="h-72 object-cover" src="<?php echo get_template_directory_uri(); ?>/assets/images/lg_dsn_estrecho.jpg" alt="Logo DSÑ" />
    </div>
    <!-- <nav class="w-full bg-dblue-900 text-white">
         <ul class="inline-flex">
           <li class="nav-pral">Principal</li>
           <li class="nav-pral">Quienes somos</li>
           <li class="nav-pral">Lo que hacemos</li>
           <li class="nav-pral">Blog</li>
           <li class="nav-pral">Redes Sociales</li>
         </ul>
       </nav> -->
  </header>
