<?php
/**
 * Template part for displaying the footer content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dsn
 */

?>


<footer class="text-white bg-gray-900 lg:grid lg:grid-cols-3">
    <!-- <aside class="hidden lg:relative lg:col-span-2 lg:block">
      <img
        src="https://images.unsplash.com/photo-1624456735729-03594a40c5fb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=668&q=80"
        alt="Comic graphic"
        class="absolute inset-0 object-cover w-full h-full"
      />
    </aside> -->

    <div class="px-4 py-16 sm:px-6 lg:px-8 lg:col-span-3">
      <div id="pie1" class="pb-12 grid grid-cols-1 gap-8 sm:grid-cols-2">
        <div >
          <p class="font-medium">
            <span class="text-xs tracking-widest uppercase">DSÑ Estudio</span>
            <!-- <a href="" class="block text-3xl">Contactanos a traves de nuestras redes:</a> -->
          </p>

          <ul class="mt-8 space-y-2 text-sm">
            <li>Puedes contactar con nosotros a traves do nuestro email.</li>
            <li>info@estudiodsn.com</li>
          </ul>
		  <ul class="mt-8 space-y-2 text-sm">
            <li>O a través de nuestras redes sociales:</li>

		  <?php
	$args = array(
		'post_type' => 'direccion',
		'order' => 'ASC',
		'orderby' => 'menu_order'
	);
	$the_query = new WP_Query( $args );
	while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<li><a href="<?php the_field('dir_url'); ?>" ><?php the_title(); ?></a></li>

	<?php endwhile; wp_reset_query(); ?>
          </ul>

          <div class="flex mt-16 space-x-3">
            <a href="#" class="text-gray-500 hover:text-gray-600" aria-label="Facebook">
              <svg class="w-6 h-6" viewBox="0 0 32 32">
                <use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-facebook"></use>
              </svg>
            </a>
            <a href="#" class="text-gray-500 hover:text-gray-600" aria-label="Instagram">
              <svg class="w-6 h-6" viewBox="0 0 32 32">
                <use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-instagram"></use>
              </svg>
            </a>
            <a href="#" class="text-gray-500 hover:text-gray-600" aria-label="Twitter">
              <svg class="w-6 h-6" viewBox="0 0 32 32">
                <use xlink:href="./assets/lib/icomoon/symbol-defs.svg#icon-twitter"></use>
              </svg>
            </a>

          </div>
        </div>

        <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
          <div>
            <p class="font-medium">Support</p>

            <nav class="flex flex-col mt-4 space-y-2 text-sm text-gray-300">
              <a href="">Coontact</a>
              <a href="">FAQs</a>
              <a href="">Live Chat</a>
              <a href="">Forums</a>
            </nav>
          </div>

          <div>
            <p class="font-medium">Products</p>

            <nav class="flex flex-col mt-4 space-y-2 text-sm text-gray-300">
              <a href="">1to1 Coaching</a>
              <a href="">Lesson Plans</a>
              <a href="">Meal Plans</a>
              <a href="">Gym Sessions</a>
            </nav>
          </div>
        </div>
      </div>

      <div class="pt-12 border-t border-gray-800">
        <div class="text-sm text-gray-300 sm:items-center sm:justify-between sm:flex">
          <div class="flex space-x-3">
            <a href="">Politica de privacidad</a>
            <!-- <a href="">Terms & Conditions</a>
            <a href="">Returns Policy</a> -->
          </div>

          <p class="mt-4 sm:mt-0">&copy; DSÑ Estudio</p>
        </div>

        <!-- <p class="mt-8 text-xs text-gray-500">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus mollitia quia quod repellendus. Porro harum,
          odio dolore perspiciatis praesentium provident esse consequatur quibusdam aperiam, cupiditate omnis modi in
          quasi? In, maxime odio vel repellat sed earum? Debitis quaerat facilis animi. Odio natus nostrum laboriosam
          impedit magnam praesentium asperiores consectetur ipsum.
        </p> -->
      </div>
    </div>
  </footer>
