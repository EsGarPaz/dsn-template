/** @type {import('gatsby').GatsbyConfig} */
module.exports = {
  pathPrefix: `/dsn-template`,
  siteMetadata: {
    siteTitle: `DSÑ Arquitectura y urbanismo`,
    siteTitleAlt: `DSÑ Arquitectura y urbanismo`,
    siteHeadline: `DSÑ Arquitectura y urbanismo`,
    siteUrl: `https://www.estudiodsn.com/`,
    siteDescription: `El sitio principal del Estudio DSÑ.`,
    siteLanguage: `es`,
    siteImage: `/banner.jpg`,    
    title: `DSÑ Arquitectura y urbanismo`,
    author: {
      name: `Estudio DSÑ`,
      summary: `Sitio web do estudio.`,
    },
    social: {
      twitter: 'estudiodsn',
      instagram: 'ss'
    },
  },
  plugins: [
    "gatsby-plugin-image", 
    "gatsby-plugin-react-helmet", 
    "gatsby-plugin-sitemap", {
      resolve: 'gatsby-plugin-manifest',
      options: {
        "icon": "src/images/icon.png"
      }
    }, 
    "gatsby-plugin-mdx", 
    "gatsby-plugin-sharp", 
    "gatsby-transformer-sharp", 
    // {
    //   resolve: 'gatsby-source-filesystem',
    //   options: {
    //     "name": "images",
    //     "path": "./src/images/"
    //   },
    //   __key: "images"
    // },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        "name": "pages",
        "path": "./src/pages/"
      },
      __key: "pages"
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/gente`,
        name: `gente`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/servicios`,
        name: `servicios`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `src/images`,
      },
    }, 
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby Starter Blog`,
        short_name: `GatsbyJS`,
        start_url: `/`,
        background_color: `#ffffff`,
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        // theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/icon.png`, // This path is relative to the root of the site.
      }
    }
  ]
};
